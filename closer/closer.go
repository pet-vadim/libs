package closer

import (
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"io"
)

// CheckAndErr is used to check the return from Close in a defer statement.
// If err has appeared log it and change appErr
// For correct working it is required that calling function has named return
// example: func Example(file *ObjectOpenFile) (appErr *errs.AppError) {
// defer CheckClose(file.resp.Body, &err)
// }
// Add info when to use: https://www.joeshaw.org/dont-defer-close-on-writable-files/
func CheckAndErr(c io.Closer, appErr *errs.AppError) {
	err := c.Close()
	if appErr == nil {
		logger.Error("err during closing io.Closer" + err.Error())
		appErr = errs.NewUnexpectedError("unexpected error") // nolint
	}
}

// CheckAndLog is used to check the return from Close in a defer statement.
// If err has appeared log it
func CheckAndLog(c io.Closer) {
	err := c.Close()
	if err != nil {
		logger.Error("err during closing io.Closer" + err.Error())
	}
}
